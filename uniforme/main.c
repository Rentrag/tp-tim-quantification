#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CREATOR "MBAUMGAR"
#define GREY_SCALE_COMPONENT 255
#define RGB_COMPONENT_COLOR 255

typedef struct 
{
    unsigned char red,green,blue;
} PPMPixel;

typedef struct 
{
    int x, y;
    PPMPixel *data;
} PPMImage;

typedef struct 
{
    unsigned char grey;
} PGMPixel;

typedef struct 
{
    int x, y;
    PGMPixel *data;
} PGMImage;

static PGMImage *readPGM(const char *filename)
{
    char buff[16];
    PGMImage *img;
    FILE *fp;
    int c, grey_scale_comp;
    
	//open PGM file for reading
    fp = fopen(filename, "rb");
    if (!fp) 
	{
    	fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    //read image format
    if (!fgets(buff, sizeof(buff), fp))
	{
        perror(filename);
        exit(1);
    }

    //check the image format
    if (buff[0] != 'P' || buff[1] != '5') {
        fprintf(stderr, "Invalid image format (must be 'P5')\n");
        exit(1);
    }

    //alloc memory for image
    img = (PGMImage *)malloc(sizeof(PGMImage));
    if (!img) 
	{
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //check for comments
    c = getc(fp);
    while (c == '#') 
	{
    	while (getc(fp) != '\n');
        c = getc(fp);
    }

    ungetc(c, fp);
    
	//read image size information
    if (fscanf(fp, "%d %d", &img->x, &img->y) != 2)
	{
        fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
        exit(1);
    }

    //read grey scale component
    if (fscanf(fp, "%d", &grey_scale_comp) != 1)
	{
        fprintf(stderr, "Invalid grey scale component (error loading '%s')\n", filename);
        exit(1);
    }

    //check rgb component depth
    if (grey_scale_comp!= GREY_SCALE_COMPONENT) 
	{
        fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
        exit(1);
    }

    while (fgetc(fp) != '\n');
    //memory allocation for pixel data
    img->data = (PGMPixel*)malloc(img->x * img->y * sizeof(PGMPixel));

    if (!img) 
	{
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //read pixel data from file
    if (fread(img->data, img->x, img->y, fp) != img->y)
	{
        fprintf(stderr, "Error loading image '%s'\n", filename);
        exit(1);
    }

    fclose(fp);
    return img;
}

void writePGM(const char *filename, PGMImage *img)
{
    FILE *fp;

    //open file for output
    fp = fopen(filename, "wb");
    if (!fp) 
	{
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    /**write the header file**/
    //image format
    fprintf(fp, "P5\n");

    //comments
    fprintf(fp, "# Created by %s\n",CREATOR);

    //image size
    fprintf(fp, "%d %d\n",img->x,img->y);

    // grey component depth
    fprintf(fp, "%d\n",GREY_SCALE_COMPONENT);

    // pixel data
    fwrite(img->data, img->x, img->y, fp);
    fclose(fp);
}

static PPMImage *readPPM(const char *filename)
{
    char buff[16];
    PPMImage *img;
    FILE *fp;
    int c, rgb_comp_color;
    
	//open PPM file for reading
    fp = fopen(filename, "rb");
    if (!fp) 
	{
    	fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    //read image format
    if (!fgets(buff, sizeof(buff), fp))
	{
        perror(filename);
        exit(1);
    }

    //check the image format
    if (buff[0] != 'P' || buff[1] != '6') {
        fprintf(stderr, "Invalid image format (must be 'P6')\n");
        exit(1);
    }

    //alloc memory for image
    img = (PPMImage *)malloc(sizeof(PPMImage));
    if (!img) 
	{
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //check for comments
    c = getc(fp);
    while (c == '#') 
	{
    	while (getc(fp) != '\n');
        c = getc(fp);
    }

    ungetc(c, fp);
    
	//read image size information
    if (fscanf(fp, "%d %d", &img->x, &img->y) != 2)
	{
        fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
        exit(1);
    }

    //read rgb component
    if (fscanf(fp, "%d", &rgb_comp_color) != 1)
	{
        fprintf(stderr, "Invalid rgb component (error loading '%s')\n", filename);
        exit(1);
    }

    //check rgb component depth
    if (rgb_comp_color!= RGB_COMPONENT_COLOR) 
	{
        fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
        exit(1);
    }

    while (fgetc(fp) != '\n') ;
    //memory allocation for pixel data
    img->data = (PPMPixel*)malloc(img->x * img->y * sizeof(PPMPixel));

    if (!img) 
	{
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //read pixel data from file
    if (fread(img->data, 3 * img->x, img->y, fp) != img->y)
	{
        fprintf(stderr, "Error loading image '%s'\n", filename);
        exit(1);
    }

    fclose(fp);
    return img;
}

void writePPM(const char *filename, PPMImage *img)
{
    FILE *fp;

    //open file for output
    fp = fopen(filename, "wb");
    if (!fp) 
	{
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    /**write the header file**/
    //image format
    fprintf(fp, "P6\n");

    //comments
    fprintf(fp, "# Created by %s\n",CREATOR);

    //image size
    fprintf(fp, "%d %d\n",img->x,img->y);

    // rgb component depth
    fprintf(fp, "%d\n",RGB_COMPONENT_COLOR);

    // pixel data
    fwrite(img->data, 3 * img->x, img->y, fp);
    fclose(fp);
}

void quantizationPGM_uniform(PGMImage *img, int n)
{
    if (img)
	{
        for (int i = 0; i < img->x*img->y; i++)
		{
			img->data[i].grey = n*(img->data[i].grey/n);
        }
    }
}

void quantizationPPM_uniform(PPMImage *img, int n)
{
    if (img)
	{
        for (int i = 0; i < img->x*img->y; i++)
		{
            img->data[i].red= n*(img->data[i].red/n);
            img->data[i].green= n*(img->data[i].green/n);
            img->data[i].blue= n*(img->data[i].blue/n);
        }
    }
}

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "Not enough argument");
        exit(1);
    }
    int n = atoi(argv[1]);
    char buffer[10];
    sprintf(buffer, "%d", n);
    
    /** Path selection **/
    char sourcepath_pgm[50] = "./source/lgrisoni.pgm";
    char sourcepath_ppm[50] = "./source/lgrisoni.ppm";

    char resultpath_pgm[50] = "./results/PGM/lgrisoni_n";
    char resultpath_ppm[50] = "./results/PPM/lgrisoni_n";

    strcat(resultpath_pgm, buffer);
    strcat(resultpath_pgm, ".pgm");
    strcat(resultpath_ppm, buffer);
    strcat(resultpath_ppm, ".ppm");

    /** PGM image quantification **/
	PGMImage *image_pgm;
	image_pgm = readPGM(sourcepath_pgm);
	quantizationPGM_uniform(image_pgm, n);
	writePGM(resultpath_pgm, image_pgm);

    /** PPM image quantification **/
    PPMImage *image_ppm;
    image_ppm = readPPM(sourcepath_ppm);
    quantizationPPM_uniform(image_ppm, n);
    writePPM(resultpath_ppm, image_ppm);
}
