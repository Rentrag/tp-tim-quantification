#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CREATOR "MBAUMGAR"
#define GREY_SCALE_COMPONENT 255
#define RGB_COMPONENT_COLOR 255

typedef struct 
{
    unsigned char red,green,blue;
} PPMPixel;

typedef struct 
{
    int x, y;
    PPMPixel *data;
} PPMImage;

typedef struct 
{
    unsigned char grey;
} PGMPixel;

typedef struct 
{
    int x, y;
    PGMPixel *data;
} PGMImage;

typedef struct
{
    int lenght;
    int data[GREY_SCALE_COMPONENT+1];
} PGMHistogram;

typedef struct
{
    int lenght;
    int data[RGB_COMPONENT_COLOR+1];
} PPMHistogram;

static PGMImage *readPGM(const char *filename)
{
    char buff[16];
    PGMImage *img;
    FILE *fp;
    int c, grey_scale_comp;
    
	//open PGM file for reading
    fp = fopen(filename, "rb");
    if (!fp) 
	{
    	fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    //read image format
    if (!fgets(buff, sizeof(buff), fp))
	{
        perror(filename);
        exit(1);
    }

    //check the image format
    if (buff[0] != 'P' || buff[1] != '5') {
        fprintf(stderr, "Invalid image format (must be 'P5')\n");
        exit(1);
    }

    //alloc memory for image
    img = (PGMImage *)malloc(sizeof(PGMImage));
    if (!img) 
	{
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //check for comments
    c = getc(fp);
    while (c == '#') 
	{
    	while (getc(fp) != '\n');
        c = getc(fp);
    }

    ungetc(c, fp);
    
	//read image size information
    if (fscanf(fp, "%d %d", &img->x, &img->y) != 2)
	{
        fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
        exit(1);
    }

    //read grey scale component
    if (fscanf(fp, "%d", &grey_scale_comp) != 1)
	{
        fprintf(stderr, "Invalid grey scale component (error loading '%s')\n", filename);
        exit(1);
    }

    //check rgb component depth
    if (grey_scale_comp!= GREY_SCALE_COMPONENT) 
	{
        fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
        exit(1);
    }

    while (fgetc(fp) != '\n');
    //memory allocation for pixel data
    img->data = (PGMPixel*)malloc(img->x * img->y * sizeof(PGMPixel));

    if (!img) 
	{
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //read pixel data from file
    if (fread(img->data, img->x, img->y, fp) != img->y)
	{
        fprintf(stderr, "Error loading image '%s'\n", filename);
        exit(1);
    }

    fclose(fp);
    return img;
}

void writePGM(const char *filename, PGMImage *img)
{
    FILE *fp;

    //open file for output
    fp = fopen(filename, "wb");
    if (!fp) 
	{
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    /**write the header file**/
    //image format
    fprintf(fp, "P5\n");

    //comments
    fprintf(fp, "# Created by %s\n",CREATOR);

    //image size
    fprintf(fp, "%d %d\n",img->x,img->y);

    // grey component depth
    fprintf(fp, "%d\n",GREY_SCALE_COMPONENT);

    // pixel data
    fwrite(img->data, img->x, img->y, fp);
    fclose(fp);
}

static PPMImage *readPPM(const char *filename)
{
    char buff[16];
    PPMImage *img;
    FILE *fp;
    int c, rgb_comp_color;
    
	//open PPM file for reading
    fp = fopen(filename, "rb");
    if (!fp) 
	{
    	fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    //read image format
    if (!fgets(buff, sizeof(buff), fp))
	{
        perror(filename);
        exit(1);
    }

    //check the image format
    if (buff[0] != 'P' || buff[1] != '6') {
        fprintf(stderr, "Invalid image format (must be 'P6')\n");
        exit(1);
    }

    //alloc memory for image
    img = (PPMImage *)malloc(sizeof(PPMImage));
    if (!img) 
	{
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //check for comments
    c = getc(fp);
    while (c == '#') 
	{
    	while (getc(fp) != '\n');
        c = getc(fp);
    }

    ungetc(c, fp);
    
	//read image size information
    if (fscanf(fp, "%d %d", &img->x, &img->y) != 2)
	{
        fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
        exit(1);
    }

    //read rgb component
    if (fscanf(fp, "%d", &rgb_comp_color) != 1)
	{
        fprintf(stderr, "Invalid rgb component (error loading '%s')\n", filename);
        exit(1);
    }

    //check rgb component depth
    if (rgb_comp_color!= RGB_COMPONENT_COLOR) 
	{
        fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
        exit(1);
    }

    while (fgetc(fp) != '\n') ;
    //memory allocation for pixel data
    img->data = (PPMPixel*)malloc(img->x * img->y * sizeof(PPMPixel));

    if (!img) 
	{
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //read pixel data from file
    if (fread(img->data, 3 * img->x, img->y, fp) != img->y)
	{
        fprintf(stderr, "Error loading image '%s'\n", filename);
        exit(1);
    }

    fclose(fp);
    return img;
}

void writePPM(const char *filename, PPMImage *img)
{
    FILE *fp;

    //open file for output
    fp = fopen(filename, "wb");
    if (!fp) 
	{
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    /**write the header file**/
    //image format
    fprintf(fp, "P6\n");

    //comments
    fprintf(fp, "# Created by %s\n",CREATOR);

    //image size
    fprintf(fp, "%d %d\n",img->x,img->y);

    // rgb component depth
    fprintf(fp, "%d\n",RGB_COMPONENT_COLOR);

    // pixel data
    fwrite(img->data, 3 * img->x, img->y, fp);
    fclose(fp);
}

void resetPGMHistogram(PGMHistogram *histo)
{
    histo->lenght = 0;
    for (int i = 0; i < GREY_SCALE_COMPONENT+1; i++)
    {
        histo->data[i] = 0;
    }
}

static PGMHistogram *fillPGMHistogram(PGMImage *img)
{
    PGMHistogram *histogram;
    histogram = (PGMHistogram *)malloc(sizeof(PGMHistogram));
    resetPGMHistogram(histogram);

    if (img)
	{
        histogram->lenght = img->x*img->y;
        for (int i = 0; i < img->x*img->y; i++)
		{
            histogram->data[img->data[i].grey]++;
        }
    }

    return histogram;
}

void resetPPMHistogram(PPMHistogram *histo)
{
    histo->lenght = 0;
    for (int i = 0; i < RGB_COMPONENT_COLOR+1; i++)
    {
        histo->data[i] = 0;
    }
}

static PPMHistogram *fillPPMHistogram(PPMImage *img)
{
    PPMHistogram *histogram;
    histogram = (PPMHistogram *)malloc(sizeof(PPMHistogram));
    resetPPMHistogram(histogram);
    
    if (img)
	{
        histogram->lenght = img->x*img->y;
        for (int i = 0; i < img->x*img->y; i++)
		{
            unsigned char grey = (0.3 * img->data[i].red) + (0.59 * img->data[i].green) + (0.11 * img->data[i].blue);
            histogram->data[grey]++;
        }
    }
    return histogram;
}

void quantizationPGM_adaptive(PGMImage *img, int m)
{
    
    // Get the image histogram
    PGMHistogram *histogram;
    histogram = fillPGMHistogram(img);
    
    // Divide the histogram into m blocs
    int min[m];
    int max[m];

    int idealBlocSize = histogram->lenght/m;
    int curGreyScale = 0;
    for (int i = 0; i < m; i++)
    {
        min[i] = curGreyScale;
        int sum = 0;
        while (sum < idealBlocSize && curGreyScale < GREY_SCALE_COMPONENT+1)
        {
            sum += histogram->data[curGreyScale];
            curGreyScale++;
        }
        max[i] = curGreyScale;
    }
    max[m-1] = 255;

    // Quantization
    if (img)
	{
        for (int i = 0; i < img->x*img->y; i++)
		{
            int j = 0;
            while (img->data[i].grey > max[j]) j++;
            img->data[i].grey = (max[j] + min[j])/2;
        }
    }
}

void quantizationPPM_adaptive(PPMImage *img, int m)
{
    
    // Get the image histogram
    PPMHistogram *histogram;
    histogram = fillPPMHistogram(img);
    
    // Divide the histogram into m blocs
    int min[m];
    int max[m];

    int idealBlocSize = histogram->lenght/m;
    int curGreyScale = 0;
    for (int i = 0; i < m; i++)
    {
        min[i] = curGreyScale;
        int sum = 0;
        while (sum < idealBlocSize && curGreyScale < RGB_COMPONENT_COLOR+1)
        {
            sum += histogram->data[curGreyScale];
            curGreyScale++;
        }
        max[i] = curGreyScale;
    }
    max[m-1] = 255;

    // Quantization
    if (img)
	{
        for (int i = 0; i < img->x*img->y; i++)
		{
            int j = 0;

            unsigned char red   = img->data[i].red;
            unsigned char green = img->data[i].green;
            unsigned char blue  = img->data[i].blue;
            unsigned char grey  = (0.30f * red) + (0.59f * green) + (0.11f * blue);

            while (grey > max[j]) j++;
            img->data[i].red    = (((max[j] + min[j])/2.0f) - (0.59f * green)    - (0.11f * blue))   / 0.30f;
            img->data[i].green  = (((max[j] + min[j])/2.0f) - (0.30f * red)      - (0.11f * blue))   / 0.59f;
            img->data[i].blue   = (((max[j] + min[j])/2.0f) - (0.30f * red)      - (0.59f * green))  / 0.11f;
            //img->data[i].red    = (grey - (0.59 * green)    - (0.11 * blue))   / 0.30;
            //img->data[i].green  = (grey - (0.30 * red)      - (0.11 * blue))   / 0.59;
            //img->data[i].blue   = (grey - (0.30 * red)      - (0.59 * green))  / 0.11;
        }
    }
}

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "Not enough argument");
        exit(1);
    }
    int m = atoi(argv[1]);
    char buffer[4];
    sprintf(buffer, "%d", m);

    /** Path selection **/
    char sourcepath_pgm[50] = "./source/lgrisoni.pgm";
    char sourcepath_ppm[50] = "./source/lgrisoni.ppm";

    char resultpath_pgm[50] = "./results/PGM/lgrisoni_m";
    char resultpath_ppm[50] = "./results/PPM/lgrisoni_m";
    
    strcat(resultpath_pgm, buffer);
    strcat(resultpath_pgm, ".pgm");
    strcat(resultpath_ppm, buffer);
    strcat(resultpath_ppm, ".ppm");

    /** PPM image quantification **/
    PGMImage *image_pgm;
    image_pgm = readPGM(sourcepath_pgm);
    quantizationPGM_adaptive(image_pgm, m);
    writePGM(resultpath_pgm, image_pgm);
    
    /** PPM image quantification **/
    PPMImage *image_ppm;
    image_ppm = readPPM(sourcepath_ppm);
    quantizationPPM_adaptive(image_ppm, m);
    writePPM(resultpath_ppm, image_ppm);
}
